+++
title = "Themagesprek 'Leven met rust en ritme' olv Esther Kouw"
author = "Esther Kouw"
image = "/uploads/leven-vanuit-rust.jpg"
header = "/uploads/leven-vanuit-rust-1.jpg"
link = "https://www.leven-vanuit-rust.nl/"
category = "themagesprek"
when = "2019-08-14T15:00:00+00:00"
+++
Ritme is terug te vinden in de natuur, de tijd, je eigen lijf. Veel mensen vinden het lastig om in balans te blijven. Er is soms veel wat er op je af kan komen. Als je uit balans bent, betekent dat eigenlijk dat je uit je ritme bent geraakt. 
Maar hoe kom je achter je eigen ritme? Hoe zorg je ervoor dat je je ritme blijft vasthouden? Wat is er dan belangrijk? 

Tijdens deze themagroep wordt er wat vertelt over de achtergronden van ritme vanuit de Benedictijnse spiritualiteit. Daar gaat het over het ritme in de dag, door o.a. het Ora et Labora (de afwisseling tussen gebed en werken), en de geloftes stabilitas, conversio morum en obedientia. 

Wat kunnen wij, mensen die buiten het klooster leven, leren van deze eeuwenoude woorden? Hoe kunnen we deze woorden naar het leven van nu vertalen? We gaan het hebben over de gelofte van conversio morum, deze leert ons meer over het belang van ritme. 

Aan de hand van eenvoudige oefeningen (o.a. stilte, reflectie en meditatieoefeningen) hopen we meer bewust te worden van ons eigen ritme. Waar kies je voor en waar niet? Hoe wil jij je dag in delen en waarom? Wat kun je doen als je uit balans bent geraakt, hoe kom je dan weer terug in je eigen ritme? We hebben allemaal gevoel voor ritme, je eigen ritme vinden vraagt moed om te stoppen en stil te staan. Wil jij vanuit rust je eigen ritme ontdekken? Sluit je dan aan bij deze interactieve themagroep!
