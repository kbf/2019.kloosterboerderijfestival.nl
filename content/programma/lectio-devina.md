+++
category = "workshop"
when = "2019-08-13T14:30:00+00:00"
image = "uploads/Broeder Bernardus.jpg"
title = "Lectio Divina"
vimeo = ""

+++
Broeder Bernardus, abt van Koningshoeven, zal op zondagmiddag van 14.30-15.30 een workshop geven over Lectio Divina.

Brother Bernardus, abbot of Abbey Koningshoeven, will give a workshop on Lectio Divina on Sunday afternoon, from 14.30-15.30
