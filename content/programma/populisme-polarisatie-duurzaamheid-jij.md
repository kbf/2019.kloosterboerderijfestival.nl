+++
category = ["workshop"]
date = ""
image = "uploads/tribalism-1201697_1280.jpg"
title = "Panel Populisme & Polarisatie"
vimeo = ""
when = "2019-08-13T16:00:00+00:00"

+++
Populisme, polarisatie. Termen waarmee we worden doodgegooid. Tegelijkertijd zijn ze superrelevant. Want wat we zien is dat er in de politiek door populisme steeds minder aandacht is voor thema’s als welzijn en duurzaamheid. Hoe herkennen we dit, wat zijn oorzaken, en wat zijn manieren om in deze tijd positief aanwezig te zijn?

Op dinsdagmiddag willen we ons daar verder in verdiepen. Ruben Altena leidt een paneldiscussie, waar Zmnako Karem en Erik Borgman zullen plaatsnemen. Gesprek, discussie, verdieping. Vanuit verschillende hoeken kijken we samen op een interactieve manier naar de politieke situatie van het moment, en proberen te begrijpen wat er gebeurt, en hoe we daarom kunnen reageren.

Zmnako Karem, bestuurskundige, betoogde in Trouw dat je als als christen alle reden hebt om op Forum voor Democratie te stemmen. Hij stelt dat het positieve geluid van Baudet over de christelijke geschiedenis en traditie van Nederland bijval verdient. Hij schreef er dit [opinieartikel over in Trouw](https://www.trouw.nl/opinie/alle-reden-voor-steun-van-christenen-aan-baudet\~bbd494a5/).

Erik Borgman is een Nederlandse leken-dominicaan en hoogleraar theologie van de religie, in het bijzonder het christendom, aan de Universiteit van Tilburg. In zijn denken is hij sterk beïnvloed door de katholieke bevrijdingstheologie. Hij stelt dat populisme niet uit xenofobie of gebrek aan integratie voortkomt, maar omgekeerd, dat de focus op de integratie het gevolg is van het populisme. Lees meer in zijn [blog op Nieuw Wij](https://www.nieuwwij.nl/opinie/800-jaar-oude-remedie-populisme/).

Ruben Altena is journalist en redacteur, die met scherpe observaties en kennis van zaken het panel zal leiden.

En wat kun jij daarmee? Wat kun jij doen/zeggen/stemmen/kiezen?