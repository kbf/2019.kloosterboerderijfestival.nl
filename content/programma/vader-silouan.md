+++
title = "Workshop iconen schilderen"
when = "2019-08-10T16:00:00+00:00"
author = "v. Silouan"
header = "/uploads/iconen-header.jpg"
image = "/uploads/icoon.png"
category = "Creatief"
youtube = "PS0Gxe0DSfc"
link = "https://www.iconenschilder.nl"
+++
Tijdens deze workshop leer je zelf de beginselen van het iconen schilderen. Onder leiding van professioneel iconenschilder Ronald Medema (Vader Silouan) werk je stap voor stap aan je eigen icoon, en in de loop van het weekend (of week) kun je deze zelf afmaken.

Maximaal 12 deelnemers
