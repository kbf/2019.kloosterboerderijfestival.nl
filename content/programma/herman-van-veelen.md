---
title: Workshop muziek
author: Herman van Veelen
category: Creatief
image: "/uploads/herman-profiel.png"
header: "/uploads/herman.jpg"
link: https://hermanvanveelen.wordpress.com
when: 2019-08-10T16:00:00+00:00

---
Herman laat zich inspireren door anti-oppressive music therapy, gregoriaanse zang en anarchistische folk punk. Ervaring met muziek niet vereist, liever niet zelfs.
