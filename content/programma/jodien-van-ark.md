+++
title = "Themagesprek Zomaar te gaan - Alledaags pelgrimeren olv Jodien van Ark"
when = "2019-08-11T15:00:00+00:00"
author = "Jodien van Ark"
header = "/uploads/jodien-header.jpg"
image = "/uploads/jodien.jpg"
category = "themagesprek"
link = "http://www.jodienvanark.nl"
+++

Jodien liep samen met Martin van Doorn naar Jeruzalem.  Ze leerden, en oefenen nog steeds, in het dagelijkse leven onderweg te zijn als pelgrim.

Haar ervaringen zijn het startpunt voor een uitwisseling over de vraag: waar trek je uit weg en waar ga je heen? wat laat je achter en wat heb je nodig voor onderweg?  hoe blijf je onderweg?

Jodien van Ark is coach, was docent aan de predikantenopleiding van de PKN en geniet van een tussentijd

