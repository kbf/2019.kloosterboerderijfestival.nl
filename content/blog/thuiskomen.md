+++
author = "Leen Maes"
date = "2019-07-21T14:00:00+00:00"
draft = false
featured_image = "/uploads/11796330_10207251452427122_6421513791951957939_n.jpg"
gallery = ["/uploads/234.jpg", "/uploads/59581508_504854726716021_8508122431448154112_o.jpg", "/uploads/IMG_20181125_110039.jpg", "/uploads/IMG_2015 (1)-1.jpg"]
title = "Thuiskomen"

+++
**Thuiskomen**

Het KloosterBoerderijFestival. Een mondvol die ik met veel liefde hardnekkig voluit blijf zeggen. Want hoe krijg je zoveel affiniteit en liefde in een afkorting van drie letters?

**Christen**

In een katholiek gezin werd ik geboren en grootgebracht. Ik kwam in heel wat christelijke middens waar ik veel leerde en ook mijn relatie met God probeerde te ontwikkelen. Een voortdurend zoeken, gevoel door mooie ontmoetingen met mooie mensen, gemeenschappen en plekken.

**Ecologie**

Tegelijk met mijn zoektocht in mijn geloof, ontwikkelde ik bewustzijn rond ecologie door een belangrijke vriend. Mijn geloof dat een constant zoeken is, inspireert me om te zijn zoals Jezus en tafels om te gooien bij onrecht. Om zorg te dragen voor deze aarde die we kregen. Om onze liefde voor de naaste, die de meeste gevolgen van de klimaatsverandering mag dragen, terwijl het voornamelijk het Westen is die hier verantwoordelijkheid voor draagt.

Met periodes van vegetarisme, veganisme, zero-waste enzoverder kon ik hier best wel hard in gaan. Soms verloor ik de nuance wat omdat ik zo graag geen kwaad wou berokkenen aan mijn naaste.

En dan vergat ik soms dat als ik vanuit liefde voor mijn naaste en de gevolgen van de klimaatsverandering voor hen, zorg wil dragen voor de planeet, dan wil ik mijn verlangen om vegetarisch te eten niet belangrijker laten zijn dan die naaste die net met veel liefde een lasagne met vlees bereidde.

**Zoeken**

In mijn zoektocht in mijn geloof, begaf ik me in christelijke middens met hele fijne mensen. Maar het internationale karakter van een nieuw beweging waar ik vaak kwam, maakte dat er zeer regelmatig, allicht zeer inspirerende en gezellige, internationale weekend in Rome of elders doorgingen. Het was zeer logisch dat iedereen daar even naartoe vloog. Ik voelde me in deze groep zeer vaak degene die moeilijk deed of er niet bij hoorde omwille van het feit dat ik, contradictorisch genoeg, net omwille van het feit dat ik mijn geloof wou beleven, niet met een hele groep even voor een weekendje het vliegtuig wou nemen naar een bijeenkomst met andere jonge christenen.

In mijn zoektocht als christen kwam ik ook uit op Taizé.   
 Op 16-jarige leeftijd nam een vriendin me voor het eerst mee en deze plek deed me iets. Dertien jaar later ben ik me nog meer bewust van de rijkdom van deze plaats.

Bij mijn vrienden die meer met ecologie bezig zijn, ben ik de enige Christen. Deze vrienden liggen me zeer nauw aan het hart, en ik voel me er echt bij thuis. Het is fijn dat we elkaar kunnen inspireren zonder dat er nadruk op ligt. Dat ze me laten nadenken over sociale rechtvaardigheid, gastvrijheid en ecologie. Over het feit dat ik Christen ben, gaat het weinig maar het is zeker geen taboe. Ik ben gewoon Leen en ik voel me zeer goed in deze groep.

**Thuiskomen**

Voor mij persoonlijk zijn mijn christenzijn enerzijds en duurzaamheid en rechtvaardigheid anderzijds twee zaken die elkaar constant voeden. De combinatie van beide, maakte dat ik me vaak de uitzondering voelde: de christen tussen mijn duurzame vrienden, de ecologische bij de christelijke groepen.

En dan kwam ik jaren geleden uit bij TimeToTurn* en SPEAK. Wat ik hier mocht proeven, was de heerlijke combinatie van zowel het christelijke als het duurzame en rechtvaardige. Nu het KloosterBoerderijFestival ontstond als project met het laatste geld dat er van TimeToTurn bij opheffing van de stichting/vzw overbleef, kwam echt alles samen.

Het heerlijk werken op het veld, groenten oosten die we dezelfde dag nog in het eten draaien en zelf brood maken zoals op Albezon en in mijn ouderlijk huis. Liefde voor al wat de natuur ons geeft.

Geloof zoals het in Taizé kan zijn: geen opsplitsingen van katholieken en protestanten maar gewoon samen naar de kern, in al zijn eenvoud. Het geloof als activerende en oncomfortabele drijfveer zoals Shane Claiborne vertelt.

Inspirerende sprekers over duurzaamheid en rechtvaardigheid, op een positieve manier en creatieve manier.

En Taizé als een rode draad door het programma geweven. Die heerlijke mantra-achtige vierstemmige gezangen en de prachtige stilte die me dichter bij mezelf en bij God brengen.

  
 Doorheen de jaren probeer ik milder en tegelijk radicaler te zijn.   
 Milder voor mezelf, de dingen in perspectief te zien. Nog steeds probeer ik mijn afval zoveel mogelijk te beperken, neem ik, mede geïnspireerd door Zomer Zonder Vliegen zo min mogelijk het vliegtuig, probeer ik lokaal te kopen en het huishouden op een ecologisch verantwoorde manier te runnen.

Waar het vroeger soms beklemmend voelde en als een groot persoonlijk falen als het niet ‘ecologisch genoeg’ was, voelt het nu, dankzij inspiratie van het KloosterBoerderijFestival, vrienden van Albezon en andere als een leuke ontdekkingstocht. Een spel waarbij ik, dankzij fijne ontmoetingen en mensen heel wat dingen bijleer en in mijn geloof kan staan.

Het is thuiskomen. Bij mezelf omdat twee delen gewoon één mogen zijn. Omdat ik op een plaats mag thuiskomen waar samen duurzaam en christen zijn een festival van ontdekking is.

Nog drie weken!   
Leen
