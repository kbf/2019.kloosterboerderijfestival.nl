+++
author = "Sofie"
date = "2019-06-04T19:44:39+00:00"
featured_image = "/uploads/IMG_7606.jpg"
gallery = ["/uploads/IMG_7606.jpg", "/uploads/P8130012.jpg", "/uploads/P7254527.jpg", "/uploads/P7245859.jpg"]
title = "Vruchtbare grond"
type = ""

+++
Alweer bijna 5 jaar geleden was ik op het allereerste KloosterBoerderijFestival. Dat was een ontzettend leuke en inspirerende week, maar voor mij ook een belangrijk moment, een startpunt van een nieuw avontuur in mijn leven. Op dat KloosterBoerderijFestival waren namelijk ook 3 anderen aanwezig, die net als ik de droom hadden om een woongemeenschap te starten waar we samen konden leven op een duurzame, gastvrije manier, met ruimte voor gebed en stilte en gericht op sociale rechtvaardigheid. We hadden elkaar eerder al leren kennen en voorzichtig wat ideeën uitgewisseld.

Tijdens de festivalweek waren we opeens allemaal samen, leefden we een week als een soort gemeenschap met elkaar in een ritme van gebed, samen werken op de boerderij, samen nadenken over het goede leven, samen eten en vieren. En daar viel voor ons het kwartje: we gaan het gewoon DOEN! We hadden tijdens het festival even geproefd aan hoe zo’n leven er een beetje uit kon zien. En het is enorm behulpzaam om in de ontmoeting met andere ‘idealisten’ weer bezield te worden waarvoor je eigenlijk echt wilt gaan. Het was het beginpunt, het besluit om met elkaar een avontuur aan te gaan. En we zitten nog steeds midden in dat avontuur. Nu, bijna 5 jaar verder zijn we met z’n vijven plus een baby, hebben we helaas nog niet een eigen gemeenschap gestart, maar leren we ontzettend veel van alle voorbereidingen daarnaartoe, groeien we als groep én wonen we alweer bijna 3 jaar met vier van de vijf in een [al bestaande leefgemeenschap](http://www.noelhuis.nl), waar we veel ervaring opdoen met wat er eigenlijk bij komt kijken, in gemeenschap leven en gastvrijheid verlenen. Zo zijn we inmiddels klaar voor de volgende stap!

Onze visie is om een gastvrije gemeenschap te zijn die vanuit een spiritualiteit van eenvoud een boodschap van barmhartigheid en rechtvaardigheid wil uitdragen. Praktisch willen we dat vertalen door een (tijdelijk) thuis te bieden aan ongedocumenteerde vluchtelingen, samen een gebedsritme te hebben, eenvoudig en duurzaam te leven, actie te voeren tegen onrecht, en anderen te inspireren om mee te doen! Als je het leuk vindt om meer te lezen over onze visie en plannen, kun je een kijkje nemen op onze website: [www.dorothygemeenschap.nl](http://www.dorothygemeenschap.nl)

En…kom vooral naar het KloosterBoerderijFestival , het is een prachtige vruchtbare plek om anderen te ontmoeten die misschien wel dezelfde idealen en dromen hebben, over hoe je iets bij kan dragen aan een mooiere wereld, hoe je je geloof in de praktijk kan brengen, hoe je een duurzame en rechtvaardige levensstijl volhoudt, hoe je rust en ritme in je leven kan inbouwen….en wie weet ontstaat er weer iets moois uit die ontmoetingen wat ook ná de zomerweek vruchtbaar blijkt!
