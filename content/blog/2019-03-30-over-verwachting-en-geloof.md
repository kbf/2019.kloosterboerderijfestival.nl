---
title: "Over Verwachting en Geloof"
date: 2019-03-30T15:16:29+01:00
featured_image: /uploads/AZ2_1132.jpg
author: Frank
type: blog
gallery: [015.jpg,AZ2_1116.jpg,IMG_8377.jpg,AZ2_1133.jpg,IMG_8103.jpg]
resources:
- name: taize
  src: 015.jpg
  params:
    caption: "taize gebed"
- name: zang
  src: AZ2_1116.jpg
  params:
    caption: "Workshop meerstemmig zang"
- name: moestuin
  src: IMG_8377.jpg
  params:
    caption: "Werken in de moestuin"
- name: gesprekken
  src: AZ2_1133.jpg
  params:
    caption: "Goede gesprekken buiten in de zon"
- name: eten
  src: IMG_8103.jpg
  params:
    caption: "Samen eten"
---
Een terugblik op hoe het KloosterBoerderijFestival 2018 voor mij was.

Zo’n 1,5 jaar geleden kwam Ronald, die ik heb leren kennen via Repair Café Overvecht, naar mij toe. Is het KloosterBoerderijFestival niets voor jou? Nooit van gehoord, maar de combinatie van (vrijwillig) werken, workshops en ontmoeting lijkt mij interessant. Bovendien ligt Nieuw Sion, de locatie van 2018, vlak bij Deventer, dus waarom niet?

Kort na deze eerste ‘ontmoeting’ met het KloosterBoerderijFestival maar eens op de website gekeken. Mijn oog viel al snel op termen als gebed, getijden, Taizé, bezinning. Dat zette mij aan het denken. Ben ik hier als niet-gelovige, maar wel met interesse voor het geloof, wel welkom? Wat ben ik dan eigenlijk wel? Atheïst, agnost, ietsist, sympathisant? God, wat ingewikkeld…Ik besluit een goede vriendin te vragen of ze mee wil. Ook haar lijkt het wel mooi om in festival stijl en middels een vast ritme met nieuwe mensen en het geloof kennis te maken.

Eenmaal op het KloosterBoerderijFestival voel ik me al heel snel thuis. Wat een plek. Wat een fijne en open mensen zijn hier. De rust, het ritme, de prachtige zang  en het ‘aarden’ in de moestuin brengt mij terug in balans. Over het geloof en geloven wordt niet uitgebreid gesproken, maar de ruimte om het er over te hebben is er zeker. Voelen doe je het ook. In het elkaar respecteren en interesse in elkaar tonen. In het respecteren van de stilte in het klooster en de tradities die er hebben geleefd. De workshops geven mij inspiratie, met het meerstemmig zingen verleg ik mijn grenzen, ik luister aandachtig naar ieders verhaal, het samen én goed eten doen mij goed.

Is dit het geloof? Eigenlijk doet het er niet toe. Ik voel me welkom.
